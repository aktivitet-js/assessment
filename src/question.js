import types from "aktivitet/types";
import events from "aktivitet/events";
import selectors from "aktivitet/selectors";

events.passed = function () { return events.base( 'passed' ) };
events.failed = function () { return events.base( 'failed' ) };

selectors.QUESTION_ANSWERS        = 'input';
selectors.QUESTION_PASSED         = '[data-pass]';
selectors.QUESTION_FAILED         = '[data-fail]';
selectors.QUESTION_ANSWERS_PASSED = selectors.QUESTION_ANSWERS + selectors.QUESTION_PASSED;
selectors.QUESTION_ANSWERS_FAILED = selectors.QUESTION_ANSWERS + selectors.QUESTION_FAILED;

class Question extends types.activity
{
    constructor( $activity, options = {}, data = {} )
    {
        options = Object.assign( {
            answers: selectors.QUESTION_ANSWERS
        }, options );

        super( $activity, options, data );

        this.is_passed = this.is_passed || false;
        this.is_failed = this.is_failed || false;
    }

    pass( complete = true )
    {
        this.is_passed = true;
        this.is_failed = false;
        this.update_classes();

        this.$activity.dispatchEvent( events.passed() );

        if ( complete )
            this.complete();
    }

    fail( complete = true )
    {
        this.is_passed = false;
        this.is_failed = true;
        this.update_classes();

        this.$activity.dispatchEvent( events.failed() );

        if ( complete )
            this.complete();
    }

    load( state )
    {
        var $ans, k, v;

        types.activity.prototype.load.call( this, state );

        if ( state.flags !== undefined )
        {
            if ( state.flags.is_passed !== undefined )
                this.is_passed = state.flags.is_passed
            if ( state.flags.is_failed !== undefined )
                this.is_failed = state.flags.is_failed
        }
        if ( state.answers !== undefined )
        {
            for ( k in state.answers )
            {
                if ( k )
                {
                    v = state.answers[ k ];
                    $ans = this.$activity.querySelector( '[name="' + k + '"]' );

                    if ( $ans.matches( '[type="checkbox"]' ) )
                    {
                        for ( var vv of v )
                        {
                            $ans = this.$activity.querySelector( '[name="' + k + '"][value="' + vv + '"]' );
                            if ( $ans )
                                $ans.checked = true;
                        }
                    }
                    else if ( $ans.matches( '[type="radio"]' ) )
                    {
                        $ans = this.$activity.querySelector( '[name="' + k + '"][value="' + v + '"]' );
                        if ( $ans )
                            $ans.checked = true;
                    }
                    else if ( $ans.matches( '[type="text"]' ) )
                        $ans.value = v;
                }
            }
        }
    }

    save()
    {
        var state = types.activity.prototype.save.call( this ),
            $$answers = this.answers(),
            k, v;

        state.flags.is_passed = this.is_passed;
        state.flags.is_failed = this.is_failed;
        state.answers = {};

        for ( const $ans of $$answers )
        {
            k = $ans.name;
            v = $ans.value;

            if ( $ans.matches( '[type="checkbox"]:checked' ) )
            {
                if ( state.answers.hasOwnProperty( k ) )
                    state.answers[ k ].push( v );
                else
                    state.answers[ k ] = [ v ];
            }
            else if ( $ans.matches( '[type="radio"]:checked,[type="text"]' ) )
            {
                state.answers[ k ] = v;
            }
        }

        return state;
    }

    update_classes()
    {
        super.update_classes();
        this.$activity.classList.toggle( 'activity-passed', this.is_passed === true );
        this.$activity.classList.toggle( 'activity-failed', this.is_failed === true );
    }

    answers( passed = null )
    {
        var sel = this.options.answers;

        if ( passed === true )
            sel += selectors.QUESTION_PASSED;
        else if ( passed === false )
            sel += selectors.QUESTION_FAILED;

        return this.$activity.querySelectorAll( sel );
    }

    check_answer( $ans )
    {
        return ! (
            ( $ans.matches( '[type="checkbox"],[type="radio"]' ) && (
                // checked fail
                (   $ans.matches( ':checked' ) && ! $ans.matches( selectors.QUESTION_PASSED ) ) ||
                // not checked pass
                ( ! $ans.matches( ':checked' ) &&   $ans.matches( selectors.QUESTION_PASSED ) )
            ) ) ||
            ( $ans.matches( '[type="text"]' ) && $ans.dataset.pass && $ans.dataset.pass !== $ans.value )
        )
    }

    handle_click( e )
    {
        if ( ! this.$activity.matches( 'form' ) )
        {
            if ( e.target.matches( '[data-pass]' ) )
                this.pass();
            else if ( e.target.matches( '[data-fail]' ) )
                this.fail();
        }
    }

    handle_submit( e )
    {
        var $$answers = this.answers();

        e.preventDefault();

        // check each answer until fail
        for ( const $ans of $$answers )
        {
            if ( ! this.check_answer( $ans ) )
            {
                this.fail();
                return false;
            }
        }

        this.pass();
        return true;
    }
}

Question.on( 'click', 'children', function ( e ) { this.handle_click( e ) } );
Question.on( 'submit', 'self', function ( e ) { this.handle_submit( e ) } );

export default Question;
