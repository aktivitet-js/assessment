import types from "aktivitet/types";

import Assessment from "./assessment.js";
import Question from "./question.js";

types.assessment = Assessment;
types.question = Question;

export { Assessment, Question };